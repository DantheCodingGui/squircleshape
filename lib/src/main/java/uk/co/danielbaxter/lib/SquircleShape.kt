package uk.co.danielbaxter.lib

import androidx.annotation.FloatRange
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.Outline
import androidx.compose.ui.graphics.Path
import androidx.compose.ui.graphics.Shape
import androidx.compose.ui.unit.Density
import androidx.compose.ui.unit.LayoutDirection

// Add https://squircley.app/ and

/**
 * A 'Squircle' shape, or super-ellipse if using the technical term
 * @param curvature How rounded you want the shape to be, tweak to your desired look
 */
class SquircleShape(@FloatRange(from = 0.0, to = 1.0) val curvature: Float = 0.7f): Shape {

    override fun createOutline(
        size: Size,
        layoutDirection: LayoutDirection,
        density: Density
    ): Outline {
        return Outline.Generic(
            path = drawOutlinePath(size, curvature)
        )
    }

    private fun drawOutlinePath(size: Size, curvature: Float): Path {
        val (width, height) = size

        val arcX = (width / 2) * (1 - curvature)
        val arcY = (height / 2) * (1 - curvature)

        return Path().apply {
            moveTo(0f, height / 2)
            cubicTo(
                0f,
                arcY,
                arcX,
                0f,
                width / 2,
                0f,
            )
            cubicTo(
                width - arcX,
                0f,
                width,
                arcY,
                width,
                height / 2,
            )
            cubicTo(
                width,
                height - arcY,
                width - arcX,
                height,
                width / 2,
                height,
            )
            cubicTo(
                arcX,
                height,
                0f,
                height - arcY,
                0f,
                height / 2,
            )
        }
    }
}