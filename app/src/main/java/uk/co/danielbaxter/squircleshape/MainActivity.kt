package uk.co.danielbaxter.squircleshape

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.PlayArrow
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.drawscope.Stroke
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import uk.co.danielbaxter.lib.SquircleShape
import uk.co.danielbaxter.squircleshape.ui.theme.SquircleShapeTheme
import kotlin.math.round

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            SquircleShapeTheme {
                // A surface container using the 'background' color from the theme
                Surface(color = MaterialTheme.colors.background) {
                    Box(Modifier.fillMaxSize()) {
                        SquircleDemo(Modifier.align(Alignment.Center))
                    }
                }
            }
        }
    }
}

@Composable
fun SquircleDemo(modifier: Modifier = Modifier) {
    var roundness by remember { mutableStateOf(0.7f) }

    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = modifier
    ) {
        BoxWithConstraints {
            Surface(
                elevation = 8.dp,
                color = MaterialTheme.colors.primary,
                shape = SquircleShape(roundness),
                modifier = Modifier
                    .padding(16.dp)
                    .size(200.dp, 100.dp)
            ) {}
        }
        Button(
            modifier = Modifier.size(36.dp),
            shape = SquircleShape(roundness),
            contentPadding = PaddingValues(0.dp),
            onClick = { },
        ) {
            Icon(Icons.Default.PlayArrow, contentDescription = null)
        }
        BoxWithConstraints {
            Surface(
                elevation = 8.dp,
                color = MaterialTheme.colors.primary,
                shape = SquircleShape(roundness),
                modifier = Modifier
                    .padding(16.dp)
                    .size(150.dp)
            ) {}
        }
        Spacer(modifier = Modifier.size(16.dp))
        Text(text = "Curvature: $roundness")
        Slider(value = roundness, onValueChange = { roundness = it })
    }
}

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    SquircleShapeTheme {
        SquircleDemo()
    }
}